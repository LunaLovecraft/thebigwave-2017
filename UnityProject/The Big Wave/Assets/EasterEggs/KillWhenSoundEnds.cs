﻿using UnityEngine;
using System.Collections;

public class KillWhenSoundEnds : MonoBehaviour {
	// Update is called once per frame
	void Update () {
		if(!(GetComponent<AudioSource>().timeSamples<GetComponent<AudioSource>().clip.samples-1)){
			Destroy(gameObject);
		}
	}
}
