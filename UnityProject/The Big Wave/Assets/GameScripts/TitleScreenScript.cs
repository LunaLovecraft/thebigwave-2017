﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(SceneTransition))]
public class TitleScreenScript : MonoBehaviour {

    public Text blinkingText;
    public int howManyFramesInBetweenBlinks;
    public string sceneToLoad;
    bool state;
    int counter;

    SceneTransition mySceneTrans;

    void Start()
    {
        counter = 0;
        if (howManyFramesInBetweenBlinks < 1)
            howManyFramesInBetweenBlinks = 1;


        mySceneTrans = GetComponent<SceneTransition>();
    }

    void Update()
    {
        counter++;

        if(counter > howManyFramesInBetweenBlinks)
        {
            counter = 0;
            state = !state;
            blinkingText.enabled = state;
        }

        if(Input.anyKey)
        {
            mySceneTrans.StartTransition(sceneToLoad);
        }
    }
   


}
