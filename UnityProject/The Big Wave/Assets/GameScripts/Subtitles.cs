﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using UnityEngine.UI;

public struct SubtitleInfo
{
    public float[] timestamps;
    public string[] subtitles;
    public int segments;
}

public class Subtitles : MonoBehaviour
{

    SubtitleInfo[] infos;

    int timeCounter;
    Text textBox;
    [SerializeField]
    public string[] FileNames;
    public int pos;
    private int prevPos;

    public Animator SubtitleAnimator;

    // Use this for initialization
    void Start()
    {
        pos = 0;
        prevPos = 0;
        textBox = this.GetComponent<Text>();
        infos = new SubtitleInfo[FileNames.Length];
        for (int i = 0; i < FileNames.Length; ++i)
        {
            TextAsset file = Resources.Load("Subtitles/" + FileNames[i]) as TextAsset;
            string[] lines = file.text.Split(new string[] { "\r\n", "\n" }, System.StringSplitOptions.RemoveEmptyEntries);
            int segments = lines.Length / 2;
            infos[i].segments = segments;
            infos[i].timestamps = new float[segments];
            infos[i].subtitles = new string[segments];
            for (int j = 0; j < lines.Length; j += 2)
            {
                infos[i].timestamps[j / 2] = float.Parse(lines[j]);
                infos[i].subtitles[j / 2] = lines[j + 1];
            }

        }
        this.textBox.text = "...";
        timeCounter = -1;
    }

    // Update is called once per frame
    void Update()
    {
        float time = Time.timeSinceLevelLoad;
        if (prevPos != pos)
        {
            prevPos = pos;
            timeCounter = -1;
            if (time > infos[pos].timestamps[0])
            {
                while (time > infos[pos].timestamps[timeCounter + 1])
                {
                    timeCounter++;
                    if (timeCounter + 2 >= infos[pos].segments)
                    {
                        break;
                    }

                }

                Debug.Log(timeCounter);
                SubtitleAnimator.SetBool("State", true);
                this.textBox.text = infos[pos].subtitles[timeCounter];
            }
            else
            {
                this.textBox.text = "...";
            }

        }
        else
        {
            if (timeCounter + 1 < infos[pos].segments)
            {
                if (time > infos[pos].timestamps[timeCounter + 1])
                {
                    timeCounter++;
                    SubtitleAnimator.SetBool("State", true);
                    if(infos[pos].subtitles[timeCounter].ToUpper() != "FIN")
                        this.textBox.text = infos[pos].subtitles[timeCounter];
                }
            }
            else
            {
                SubtitleAnimator.SetBool("State", false);
            }
        }
    }
}
