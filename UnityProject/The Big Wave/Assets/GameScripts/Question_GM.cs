﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Question_GM : MonoBehaviour {
    public static GameState TestState;
    bool sceneLoaded = false;
    bool inputAccepted = false;
    public GameObject NotesField;
	// Use this for initialization
	void Start () {
        this.NotesField.GetComponent<InputField>().text = GameInfo.Notes;
		Question_GM.TestState = TestState;
    }
	
	// Update is called once per frame
	void Update () {
        if (!sceneLoaded)
        {
            sceneLoaded = true;
            LoadScene();
        }
        if (Input.GetKeyDown("0"))
        {
            AcceptInput(0);
        }
        else if (Input.GetKeyDown("1"))
        {
            AcceptInput(1);
        }
        else if (Input.GetKeyDown("2"))
        {
            AcceptInput(2);
        }
        else if (Input.GetKeyDown("3"))
        {
            AcceptInput(3);
        }
        else if (Input.GetKeyDown("4"))
        {
            AcceptInput(4);
        }
        else if (Input.GetKeyDown("5"))
        {
            AcceptInput(5);
        }
        else if (Input.GetKeyDown("6"))
        {
            AcceptInput(6);
        }
        else if (Input.GetKeyDown("7"))
        {
            AcceptInput(7);
        }
        else if (Input.GetKeyDown("8"))
        {
            AcceptInput(8);
        }
        else if (Input.GetKeyDown("9"))
        {
            AcceptInput(9);
        }

        switch (GameInfo.CurrentState)
        {
            case GameState.Scene1:

                break;
            case GameState.Scene2:

                break;
            case GameState.Scene3:

                break;
        }
    }

    void LoadScene()
    {
        switch (GameInfo.CurrentState)
        {
            case GameState.Scene1:
                AudioManager.Instance.SetState(0);
                break;
            case GameState.Scene2:
                AudioManager.Instance.SetState(1);
                break;
            case GameState.Scene3:
                AudioManager.Instance.SetState(2);
                break;
            default:
                GameInfo.CurrentState = TestState;
                LoadScene();
                break;
        }

        
    }
    
    public void UpdateNotes(string change)
    {
        GameInfo.Notes = change;
    }

    public void AcceptInput(int input)
    {
        if (inputAccepted)
            return;

        AudioManager.Instance.SetState(3);


        switch (GameInfo.CurrentState)
        {
            case GameState.Scene1:
                switch (input)
                {
                    case 3:
                        //Winner
                        this.GetComponent<SceneTransition>().StartTransition("Scene2");
                        break;
                    default:
                        //Loser
                        GameInfo.losing = true;
                        this.GetComponent<SceneTransition>().StartTransition("GameLost");
                        
                        break;
                }
                break;
            case GameState.Scene2:
                switch (input)
                {
                    case 4:
                        //Winner
                        this.GetComponent<SceneTransition>().StartTransition("Scene3");
                        break;
                    default:
                        //Loser
                        GameInfo.losing = true;
                        this.GetComponent<SceneTransition>().StartTransition("GameLost");
                        break;
                }
                break;
            case GameState.Scene3:
                switch (input)
                {
                    case 3:
                        //Winner
                        this.GetComponent<SceneTransition>().StartTransition("GameWon");
                        break;
                    default:
                        //Loser
                        GameInfo.losing = true;
                        this.GetComponent<SceneTransition>().StartTransition("GameLost");
                        break;
                }
                break;
        }


    }
}
