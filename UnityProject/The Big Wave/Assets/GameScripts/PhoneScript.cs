﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneScript : MonoBehaviour
{

    public Text OutputText;
    public Animator PhoneAnimator;
    public int TimeTillPhoneCanBeUsed;
    float curTime;
    bool togglingUnderscore;
    bool state;
    int toggleCount = 0;
    string currentstring = "";




    // Use this for initialization
    void Start()
    {
        curTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // Edit the time till next scene
        curTime += Time.deltaTime;
        if(curTime > TimeTillPhoneCanBeUsed)
        {
            // Do something
            PhoneAnimator.SetBool("CallWaiting", true);
        }


        OutputText.text = currentstring;
        if (state)
        {
            toggleCount++;
            if (toggleCount > 100)
            {
                togglingUnderscore = !togglingUnderscore;
                toggleCount = 0;
            }

            if (!togglingUnderscore)
                OutputText.text += "_";
            else
                OutputText.text += " ";
        }
        else
            currentstring = "";

        if (PhoneAnimator)
            PhoneAnimator.SetBool("State", state);
    }

    public void AddInput(string _input)
    {
        if (state)
            currentstring += _input;
    }

    public void TogglePhoneState()
    {
        if(curTime > TimeTillPhoneCanBeUsed)
        {
            state = !state;
        }
    
    }
}
