﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class RedlightGreenlightScript : MonoBehaviour
{

    public Text TextToWatch;
    bool State;

    string[] ValidEntries = { "88.0", "91.2", "93.9", "95.2", "105.9" };
    Animator myAnimator;

    void Start()
    {
        myAnimator = GetComponent<Animator>();
    }


    // Update is called once per frame
    void Update()
    {
        // If it's technically correct you should show the green light
        foreach (string s in ValidEntries)
        {
            if (s == TextToWatch.text)
            {
                myAnimator.SetBool("State", true);
                return;
            }
        }

        // Otherwise it should show the red light
        myAnimator.SetBool("State", false);

    }
}
