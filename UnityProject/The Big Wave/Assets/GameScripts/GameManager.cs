﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public GameState SceneState;
	public InputField inputField;
    public GameObject NotesField;
    bool stageEnding = false;

    // Use this for initialization
    void Start () {
        if (GameInfo.resetLoss)
        {
            GameInfo.losing = false;
        }
        if (!GameInfo.losing&&!GameInfo.resetLoss)
        {
            if(SceneState != GameState.GameWon && SceneState != GameState.MainMenu)
                GameInfo.CurrentState = SceneState;
        }
        this.NotesField.GetComponent<InputField>().text = GameInfo.Notes;
    }
	
	// Update is called once per frame
	void Update () {
        if (!stageEnding && GameInfo.CurrentState != GameState.GameWon && !GameInfo.losing)
        {
            if(Time.timeSinceLevelLoad > 60.0f)
            {
                stageEnding = true;
                this.GetComponent<SceneTransition>().StartTransition("Question");
            }
        }
	}

    public void UpdateNotes(string change)
    {
        GameInfo.Notes = change;
    }

    public void ResetLevel()
    {
        GameInfo.resetLoss = true;
        GameInfo.losing = false;
        switch (GameInfo.CurrentState)
        {
            case GameState.Scene1:
                this.GetComponent<SceneTransition>().StartTransition("Scene1");
                break;

            case GameState.Scene2:
                this.GetComponent<SceneTransition>().StartTransition("Scene2");
                break;

            case GameState.Scene3:
                this.GetComponent<SceneTransition>().StartTransition("Scene3");
                break;

        }
    }

    public void ChangeChannel(string newChannel)
    {
        if (newChannel == "97" || newChannel == "97.")
            newChannel = "97.0";
        if (newChannel == "88" || newChannel == "88.")
            newChannel = "88.0";

        if (newChannel.Contains("-")||
			newChannel.Contains("\\")||
			newChannel.Contains("/")||
			newChannel.Contains(",")||
			newChannel.Contains("(")||
			newChannel.Contains(")")||
			newChannel.Contains("+")||
			newChannel.Contains("_")||
			newChannel.Contains("^")||
			newChannel.Contains("!")||
			newChannel.Contains("~")||
			newChannel.Contains("'")||
			newChannel.Contains("\"")||
			newChannel.Contains("@")||
			newChannel.Contains("#")||
			newChannel.Contains("$")||
			newChannel.Contains("%")||
			newChannel.Contains("&")||
			newChannel.Contains("*")||
			newChannel.Contains("[")||
			newChannel.Contains("]")||
			newChannel.Contains("|")||
			newChannel.Contains("<")||
			newChannel.Contains(">")||
			newChannel.Contains("?")||
			newChannel.Contains(":")||
			newChannel.Contains(";")||
			newChannel.Contains("{")||
			newChannel.Contains("}")){
			newChannel = "0.0";
		}
		if (newChannel == "." || newChannel == " . " || newChannel == ". " || newChannel == " ." || newChannel == "") {
			newChannel = "0.0";
		}
		string channelText = Convert.ToDouble (newChannel).ToString();
		string[] channelTextSegments = (channelText.Split('.'));
		if (newChannel == "" || newChannel == " ")
			channelText = AudioManager.Instance.GetState ();
		if (channelTextSegments.Length == 1)
			channelText = channelText + ".0";
		if (channelText.Length == 2){
            char c = (char)Convert.ToInt16(channelTextSegments[1]);
            if (c == ' ' || c == 0) {
                c = '0';
            }

            channelText = String.Format ("{0}.{1}",
				channelTextSegments[0],c);
		}
        if (Convert.ToDouble(channelText) < 0.01)
        {
            channelText = 0.ToString("0.0");
        }

        if (channelText == "97" || channelText == "97.")
            channelText = "97.0";
        if (channelText == "88" || channelText == "88.")
            channelText = "88.0"; 
        // Debug.Log(Convert.ToDouble(channelText));

        inputField.text = channelText;
        for (int i = AudioManager.Instance.State_Labels.Length; i > 0; )
        {
            i--;
            if(AudioManager.Instance.State_Labels[i] == newChannel)
            {
                AudioManager.Instance.SetState(channelText);
                Debug.Log(channelText);
                return;
            }
        }
		AudioManager.Instance.SetState("0");
		// Debug.Log(inputField.text);
		// Debug.Log(channelText);
    }
}
