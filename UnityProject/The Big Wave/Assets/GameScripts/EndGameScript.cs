﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameScript : MonoBehaviour
{

    public Canvas canvasToBeMoved;
    public SpriteRenderer Congrats;
    public float scrollingSpeed;
    float startval = 4;
    bool scrolling = false;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(waittoStartScrollingCredits());
    }

    // Update is called once per frame
    void Update()
    {
        startval -= .01f;
        Color fadedColor = Congrats.color;
        fadedColor.a = startval;
        Congrats.color = fadedColor;

        if (scrolling)
        {
            Vector3 pos = canvasToBeMoved.transform.position;
            pos.y += scrollingSpeed;
            canvasToBeMoved.transform.position = pos;
        }
    }


    IEnumerator waittoStartScrollingCredits()
    {
        yield return new WaitForSeconds(10);
        scrolling = true;
        StartCoroutine(waittoEndGame());
    }

    IEnumerator waittoEndGame()
    {
        yield return new WaitForSeconds(45);
        SceneManager.LoadScene("MainMenu");
    }



}
