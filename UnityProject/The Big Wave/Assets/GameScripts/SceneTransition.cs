﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    public SpriteRenderer StaticRenderer;
    public SpriteRenderer blackRenderer;
    public bool SceneTransitionSwitch;
    bool costart;
    string toScene;
    float timeSinceStart = 0;
    Color clear = new Color(1, 1, 1, 0);

    void FixedUpdate()
    {
        if (SceneTransitionSwitch)
        {
            Color currentColor = StaticRenderer.color;
            currentColor.a += .01f;
            StaticRenderer.color = currentColor; // Unity wont let us directly modify the alpha value

            if(StaticRenderer.color.a >= 1)
            {
                Color blackColor = blackRenderer.color;
                blackColor.a += .01f;
                blackRenderer.color = blackColor;

                if (blackRenderer.color.a >= 1)
                {
                    // goto the new scene
                    if (!costart)
                    {
                        costart = true;
                        StopCoroutine("waitcoroutine");
                        StartCoroutine(waitcoroutine());
                    }
                    else
                        timeSinceStart += Time.fixedDeltaTime;
                    if(timeSinceStart>5.0f)
                        SceneManager.LoadScene(toScene);
                }
            }
        }
        else
            StaticRenderer.color = clear;
    }

    public void StartTransition(string _sceneName)
    {
        toScene = _sceneName;
        SceneTransitionSwitch = true;
    }

    IEnumerator waitcoroutine()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(toScene);
    }
}
