﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    MainMenu,
    Scene1,
    Scene2,
    Scene3,
    GameWon
}

public class GameInfo
{
    public static GameState CurrentState = GameState.MainMenu;
    public static string Notes = "Higher ups want me to figure out what's going down using the HAM Radio. I have a few channels written down but I'll have to input any others by hand. I should write on this slab of paper any info I come by. (Click me to edit)";
    public static bool losing = false;
    public static bool resetLoss = false;
}
