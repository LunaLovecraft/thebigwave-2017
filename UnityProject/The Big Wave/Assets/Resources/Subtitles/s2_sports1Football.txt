0.0
Eddy: Wow! Would you look at that crowd! The tension is electric today!
4.0
Joe: Well, of course it is, Eddy! This is the first game these two have played since their 2004 championship!
10.5
Eddy: Really! That game was only 19 to 34, but they made each other fight for every point!
17.0
Joe: It was like the field was exploding...Ah! Here they come! Kickoff...and they're off!
24.0
Eddy: Number 14 has the ball, passes to 27, here they go! First down!
28.5
Joe: At the 20 yard line!
30.5
Eddy: You know, that reminds me of when number 21 tore his achilles tendon!  
35.5
Joe: Oh yeah, back in 19...what was it?
40.0
Eddy: 88.
42.0
Joe: Oh yes, 1988, a year to remember!
44.0
Eddy: Absolutely! 1988 was their top year, you know.
50.0
Joe: Even without 21, they still slammed their competition!
53.0
Eddy: Too true, Joe! Oh! They�re on again! Defense is lining up for some serious plays.
57.0
Joe: And they'll have to Eddy, numbers 8 and 15 look ready to rage.
63.0
fin