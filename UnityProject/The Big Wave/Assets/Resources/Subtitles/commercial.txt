0.0
Mr. Moustache: Howdy!  I'm Mr. Moustache, proprietor of Mr. Mustache's Mustache Emporium!
7.0
Mr. Moustache: And I must-ache you a question!
10.0
Mr. Moustache: Are you ready for the trip of a lifetime?
13.0
Mr. Moustache: Here at the Emporium we're always on the up and up--
15.0
Mr. Moustache: and so is our building!
17.0
Mr. Moustache: We're proud to announce a complete remodeling to browse our bristles.
21.0
Mr. Moustache: Check out our new hub, located on the 2nd floor with our new
24.5
Mr. Moustache: Mustache-chic furniture collection!
27.0
Mr. Moustache: Guaranteed to meet all of your furnishing needs with a fashionable furry twist!
31.0
Mr. Moustache: Or maybe you have a thing for our 4th floor,
35.0
Mr. Moustache: the mustache museum!  Come marvel at our new exhibit--
38.0
Mr. Moustache: Mustaches of History! See
41.0
Mr. Moustache: Mussolini's famous lip warmer, recreated in marvelous mustache wax.
45.5
Mr. Moustache: All this and more, here at Mr. Mustaches Mustache Emporium.
50.0
Mr. Moustache: Come on down!
55.0
fin
