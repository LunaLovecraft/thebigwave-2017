0.0
Jim: If you're tuning in just now you're witnessing the soccer world cup semifinals between Italy and Argentina! 
6.0
How do you think Shooster's doing today Bobby?
9.0
Bobby: He seems to have recovered from his recent slump.
11.5
Jim: Well, he�s certainly playing well tonight!
14.0
Bobby: Off they go!  It�s Shooster to Lengert, passes to Woodward! 
17.0
Bobby: Kicked out to Jameson!
19.0
Jim: And it's Jameson with the shot... And it's in! GOAL! JAMESON HAS DONE IT!
24.0
Bobby:  I haven't seen a play like that since '97!
28.0
Jim: Yes 1997... What happened in 1997?
31.0
Bobby: That play, the amazing play that happened in '97!!
37.0
Jim:  What play, though?
39.0
Bobby:  Oh!  Looks like Campito's gearing up for the shot!
42.0
Jim:  He's been 0 for 3 today, let�s see if he can make something happen.
45.0
fin
